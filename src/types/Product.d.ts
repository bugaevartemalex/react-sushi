export type productType = {
    id: number;
    imageUrl: string;
    name: string;
    types: number[];
    sizes: number[];
    price: number;
    category: number;
    rating: number;
    onAddItem: (item: any) => void;
    addedCount: number;
};
