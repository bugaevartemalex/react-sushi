export type cartItemType = {
    id: number;
    imageUrl: string;
    name: string;
    price: number;
    size: number;
    type: string;
    onRemove: (el: any) => void;
};
