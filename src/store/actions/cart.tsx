//@ts-nocheck
import {
    ADD_PRODUCTS_TO_CART,
    REMOVE_PRODUCTS_TO_CART,
    CLEAR_CART,
    TOTAL_COUNT_CART,
    TOTAL_PRICE_CART,
} from './actionsTypes';

export function toCart(item) {
    return async (dispatch: Dispatch): Promise<void> => {
        dispatch(addProductToCart(item));
        dispatch(totalCount());
        dispatch(totalPrice());
    };
}

export function addProductToCart(item: any) {
    return {
        type: ADD_PRODUCTS_TO_CART,
        payload: item,
    };
}

export function removeProductToCart(item: any) {
    return {
        type: REMOVE_PRODUCTS_TO_CART,
        payload: item,
    };
}

export function clearCart() {
    return {
        type: CLEAR_CART,
    };
}

function totalCount() {
    return {
        type: TOTAL_COUNT_CART,
    };
}
function totalPrice() {
    return {
        type: TOTAL_PRICE_CART,
    };
}
