import axios from 'axios';
import { Dispatch } from 'redux';
import { FETCH_PRODUCTS_START, FETCH_PRODUCTS_SUCCESS } from './actionsTypes';
import { productType } from '../../types/Product';

export function fetchProduct(sortBy: string, category: number) {
    return async (dispatch: Dispatch): Promise<void> => {
        dispatch(fetchProductsStart());
        try {
            const response = await axios.get(
                `/products?${category !== null ? `category=${category}` : ''}&_sort=${sortBy}&_order=desc`
            );

            dispatch(fetchProdutsSuccess(response.data));
        } catch (ex) {
            console.error(ex);
        }
    };
}
export function fetchProductsStart() {
    return {
        type: FETCH_PRODUCTS_START,
    };
}

export function fetchProdutsSuccess(products: productType) {
    return {
        type: FETCH_PRODUCTS_SUCCESS,
        payload: products,
    };
}
