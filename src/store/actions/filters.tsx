import { FILTER_PRODUCTS, SORT_PRODUCTS } from './actionsTypes';

export function filterProducts(type: number | null) {
    return {
        type: FILTER_PRODUCTS,
        payload: type,
    };
}
export function sortProducts(type: string) {
    return {
        type: SORT_PRODUCTS,
        payload: type,
    };
}
