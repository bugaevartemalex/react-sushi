import { FILTER_PRODUCTS, SORT_PRODUCTS } from '../actions/actionsTypes';

type sortBy = 'popular' | 'name' | 'price';

type filterStore = {
    category: number | null;
    sortBy: sortBy;
};
type types = typeof FILTER_PRODUCTS | typeof SORT_PRODUCTS;

type filterActions = {
    type: types;
    payload: number | string;
};

const initialState: filterStore = {
    category: null,
    sortBy: 'popular',
};

export default function filterReducer(state = initialState, action: filterActions) {
    switch (action.type) {
        case FILTER_PRODUCTS:
            return {
                ...state,
                category: action.payload,
            };
        case SORT_PRODUCTS:
            return {
                ...state,
                sortBy: action.payload,
            };
        default:
            return state;
    }
}
