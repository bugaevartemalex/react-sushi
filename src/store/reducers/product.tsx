import { FETCH_PRODUCTS_START, FETCH_PRODUCTS_SUCCESS } from '../actions/actionsTypes';
import { productType } from '../../types/Product';

type ProdutsStore = {
    products: productType[];
    loading: boolean;
};

type types = typeof FETCH_PRODUCTS_START | typeof FETCH_PRODUCTS_SUCCESS;

type ProductsAction = {
    type: types;
    payload?: productType;
};

const initialState: ProdutsStore = {
    products: [],
    loading: false,
};

export default function productReducer(state = initialState, action: ProductsAction) {
    switch (action.type) {
        case FETCH_PRODUCTS_START:
            return {
                ...state,
                loading: true,
            };
        case FETCH_PRODUCTS_SUCCESS:
            return {
                ...state,
                loading: false,
                products: action.payload,
            };
        default:
            return state;
    }
}
