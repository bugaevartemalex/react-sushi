//@ts-nocheck
import {
    ADD_PRODUCTS_TO_CART,
    REMOVE_PRODUCTS_TO_CART,
    CLEAR_CART,
    TOTAL_COUNT_CART,
    TOTAL_PRICE_CART,
} from '../actions/actionsTypes';

const initialState = {
    items: {},
    totalCount: 0,
    totalPrice: 0,
};

export default function cartReducer(state = initialState, action: any) {
    const itemsArr = Object.values(state.items).flat();
    switch (action.type) {
        case ADD_PRODUCTS_TO_CART:
            const items = {
                ...state.items,
                [action.payload.id]: !state.items[action.payload.id]
                    ? [action.payload]
                    : [...state.items[action.payload.id], action.payload],
            };
            return {
                ...state,
                items,
            };
        case REMOVE_PRODUCTS_TO_CART:
            return {
                ...state,
            };
        case TOTAL_COUNT_CART:
            const totalCount = itemsArr.length;
            return {
                ...state,
                totalCount,
            };
        case TOTAL_PRICE_CART:
            const totalPrice = itemsArr.reduce((total, cur) => cur.price + total, 0);
            return {
                ...state,
                totalPrice,
            };
        case CLEAR_CART:
            return {
                items: {},
                totalCount: 0,
                totalPrice: 0,
            };
        default:
            return state;
    }
}
