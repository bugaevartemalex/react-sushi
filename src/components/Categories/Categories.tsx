import React from 'react';
//local store
const categories: string[] = ['Sushi', 'Rolls', 'Sets', 'Soup'];
//types
type cagtegoryProps = {
    activeCategory: number;
    onClickItem: (index: number | null) => void;
};

const Categories: React.FC<cagtegoryProps> = React.memo(({ activeCategory, onClickItem }) => {
    const onSelectCategory = (index: number | null) => {
        onClickItem(index);
    };

    // console.log('RERENDER CATEGORY');
    return (
        <div className="categories">
            <ul>
                <li className={activeCategory === null ? 'active' : ''} onClick={() => onSelectCategory(null)}>
                    Все
                </li>
                {categories?.map((item, index) => {
                    return (
                        <li
                            className={activeCategory === index ? 'active' : ''}
                            onClick={() => onSelectCategory(index)}
                            key={`${item}_${index}`}
                        >
                            {item}
                        </li>
                    );
                })}
            </ul>
        </div>
    );
});

export default Categories;
