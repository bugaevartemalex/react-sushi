type buttonProp = {
    onClick?: () => void;
    type?: string;
};

const Button: React.FC<buttonProp> = ({ onClick, type, children }) => {
    return (
        <button onClick={onClick} className={`button button--${type}`}>
            {children}
        </button>
    );
};
export default Button;
