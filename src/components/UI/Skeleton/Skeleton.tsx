import Skeleton from 'react-loading-skeleton';
const SkeletonCard: React.FC = () => {
    return (
        <>
            {Array(9)
                .fill(0)
                .map((_, index) => (
                    <li className="pizza-block" key={index}>
                        <Skeleton circle={true} width={220} height={220} />
                        <h4 className="pizza-block__title">
                            <Skeleton height={24} width={`100%`} />
                        </h4>
                        <div className="pizza-block__selector">
                            <Skeleton width={`100%`} height={74} />
                        </div>
                        <div className="pizza-block__bottom">
                            <Skeleton width={`100%`} height={34} />
                        </div>
                    </li>
                ))}
        </>
    );
};
export default SkeletonCard;
