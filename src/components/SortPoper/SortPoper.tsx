import React, { useEffect, useRef, useState } from 'react';

// types
import { sortByType } from '../../types/Sort';
//local store
const sortItem: sortByType[] = [
    {
        name: 'popular',
        type: 'popular',
    },
    {
        name: 'price',
        type: 'price',
    },
    {
        name: 'name',
        type: 'name',
    },
];
type sortProps = {
    sortBy: string;
    onClickItem: (item: sortByType) => void;
};

const Sort: React.FC<sortProps> = React.memo(({ sortBy, onClickItem }) => {
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const sortRef = useRef<HTMLDivElement | null>(null);

    const toggleIsOpen = () => {
        setIsOpen(!isOpen);
    };

    const chooseSortType = (item: sortByType) => {
        onClickItem(item);
        setIsOpen(false);
    };

    const handleOutsideClick = (event: any) => {
        const path = event.path || (event.composedPath && event.composedPath());
        if (!path.includes(sortRef.current)) {
            setIsOpen(false);
        }
    };

    useEffect(() => {
        document.body.addEventListener('click', handleOutsideClick);
    }, []);
    // console.log('RERENDER SORT');
    return (
        <div ref={sortRef} className="sort">
            <div className="sort__label">
                <svg
                    className={isOpen ? '' : 'rotate'}
                    width="10"
                    height="6"
                    viewBox="0 0 10 6"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        d="M10 5C10 5.16927 9.93815 5.31576 9.81445 5.43945C9.69075 5.56315 9.54427 5.625 9.375 5.625H0.625C0.455729 5.625 0.309245 5.56315 0.185547 5.43945C0.061849 5.31576 0 5.16927 0 5C0 4.83073 0.061849 4.68424 0.185547 4.56055L4.56055 0.185547C4.68424 0.061849 4.83073 0 5 0C5.16927 0 5.31576 0.061849 5.43945 0.185547L9.81445 4.56055C9.93815 4.68424 10 4.83073 10 5Z"
                        fill="#2C2C2C"
                    />
                </svg>
                <b>sort by:</b>
                <span onClick={toggleIsOpen}>{sortBy}</span>
            </div>
            {isOpen && (
                <div className="sort__popup">
                    <ul>
                        {sortItem?.map((item: sortByType, index: number) => {
                            return (
                                <li
                                    className={sortBy === item.type ? 'active' : ''}
                                    onClick={() => chooseSortType(item)}
                                    key={`${item.name}_${index}`}
                                >
                                    {item.name}
                                </li>
                            );
                        })}
                    </ul>
                </div>
            )}
        </div>
    );
});

export default Sort;
