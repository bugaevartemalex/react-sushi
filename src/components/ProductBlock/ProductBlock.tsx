import { useState } from 'react';
import { productType } from '../../types/Product';
import Button from '../../components/UI/Button/Button';

const ProductBlock: React.FC<productType> = ({ id, name, imageUrl, sizes, price, types, onAddItem, addedCount }) => {
    const [typesNames] = useState(['small', 'standard']);
    const [activeType, setActiveType] = useState(types[0]);
    const [activeSize, setActiveSize] = useState(sizes[0]);
    const [currentPrice, setCurrentPrice] = useState(price);

    const onSelectTypes = (i: number) => {
        setActiveType(i);
    };
    const onSelectSize = (i: number) => {
        setActiveSize(i);
        setCurrentPrice((price * i) / sizes[0]);
    };
    const onAddProduct = () => {
        const obj = {
            id,
            name,
            imageUrl,
            size: activeSize,
            price: currentPrice,
            type: typesNames[activeType],
        };
        onAddItem(obj);
    };
    return (
        <div className="pizza-block">
            <img className="pizza-block__image" src={imageUrl} alt={name} />
            <h4 className="pizza-block__title">{name}</h4>
            <div className="pizza-block__selector">
                <ul>
                    {typesNames?.map((type, i: number) => {
                        const classes = []; // activeType === i ? 'active' : !types.includes(i) ? 'disabled' : ''
                        if (activeType === i) {
                            classes.push('active');
                        } else if (!types.includes(i)) {
                            classes.push('disabled');
                        }
                        return (
                            <li className={classes.join('')} onClick={() => onSelectTypes(i)} key={`${type}_${i}`}>
                                {type}
                            </li>
                        );
                    })}
                </ul>
                <ul>
                    {sizes?.map((size, i) => {
                        return (
                            <li
                                className={activeSize === size ? 'active' : !sizes.includes(size) ? 'disabled' : ''}
                                onClick={() => onSelectSize(size)}
                                key={`${size}_${i}`}
                            >
                                {size} item
                            </li>
                        );
                    })}
                </ul>
            </div>
            <div className="pizza-block__bottom">
                <div className="pizza-block__price">{currentPrice} ₽</div>

                <Button type="outline button--add" onClick={onAddProduct}>
                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M10.8 4.8H7.2V1.2C7.2 0.5373 6.6627 0 6 0C5.3373 0 4.8 0.5373 4.8 1.2V4.8H1.2C0.5373 4.8 0 5.3373 0 6C0 6.6627 0.5373 7.2 1.2 7.2H4.8V10.8C4.8 11.4627 5.3373 12 6 12C6.6627 12 7.2 11.4627 7.2 10.8V7.2H10.8C11.4627 7.2 12 6.6627 12 6C12 5.3373 11.4627 4.8 10.8 4.8Z"
                            fill="white"
                        />
                    </svg>
                    <span>Add</span>
                    <i>{addedCount}</i>
                </Button>
            </div>
        </div>
    );
};
export default ProductBlock;
