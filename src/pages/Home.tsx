import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
//redux actions
import { fetchProduct } from '../store/actions/products';
import { filterProducts } from '../store/actions/filters';
import { sortProducts } from '../store/actions/filters';
import { toCart } from '../store/actions/cart';
//components
import { Categories, Sort, ProductBlock, SkeletonCard } from '../components';
//types
import { productType } from '../types/Product';
import { sortByType } from '../types/Sort';

const Home: React.FC = () => {
    const dispatch = useDispatch();
    // @ts-ignore
    const { products, loading, category, sortBy, cartItem } = useSelector(({ products, filters, cart }) => {
        return {
            products: products.products,
            loading: products.loading,
            category: filters.category,
            sortBy: filters.sortBy,
            cartItem: cart.items,
        };
    });

    useEffect(() => {
        dispatch(fetchProduct(sortBy, category));
    }, [dispatch, sortBy, category]);

    const filterProductsHandler = useCallback(
        (index: number | null) => {
            if (index !== category) dispatch(filterProducts(index));
        },
        [dispatch, category]
    );
    const SortProductsHandler = useCallback(
        (item: sortByType) => {
            if (item.name !== sortBy) dispatch(sortProducts(item.name));
        },
        [dispatch, sortBy]
    );
    const onAddProductToCart = (obj: any) => {
        dispatch(toCart(obj));
    };
    return (
        <div className="container">
            <div className="content__top">
                <Categories activeCategory={category} onClickItem={filterProductsHandler} />
                <Sort sortBy={sortBy} onClickItem={SortProductsHandler} />
            </div>
            <h2 className="content__title">All products</h2>
            {loading ? (
                <div className="content__items">
                    <SkeletonCard />
                </div>
            ) : (
                <div className="content__items">
                    {products?.map((item: productType, index: number) => {
                        return (
                            <ProductBlock
                                {...item}
                                key={`${item.name}_${index}`}
                                addedCount={cartItem[item.id] ? cartItem[item.id].length : 0}
                                onAddItem={onAddProductToCart}
                            />
                        );
                    })}
                </div>
            )}
        </div>
    );
};

export default Home;
