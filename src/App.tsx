import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
//components
import { Header } from './components';
//pages
import { Home, Cart } from './pages';

const App: React.FC = () => {
    return (
        <BrowserRouter>
            <div className="wrapper">
                <Header />
                <div className="content">
                    <Switch>
                        <Route component={Home} path="/" exact></Route>
                        <Route component={Cart} path="/cart" exact></Route>
                    </Switch>
                </div>
            </div>
        </BrowserRouter>
    );
};

export default App;
